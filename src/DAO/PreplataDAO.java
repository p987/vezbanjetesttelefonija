package DAO;

import java.util.Collection;

import model.Predplata;

public interface PreplataDAO {
	
	public void add(Predplata karta) throws Exception;
	public Collection<Predplata> getAll() throws Exception;
	
}
