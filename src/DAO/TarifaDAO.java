package DAO;

import java.util.Collection;

import model.Tarifa;

public interface TarifaDAO {

	public Tarifa get(long brojVoza) throws Exception;

	public Collection<Tarifa> getAll() throws Exception;

}
