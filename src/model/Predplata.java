package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import util.Konzola;


public class Predplata {

	private final long id;
	private Tarifa tarifa;
	private String preplatnickiBroj;
	private LocalDate datumPocetka;
	private int trajanjeUgovora; //12 24 36 meseci
	
	
	public boolean isDatumUOpsegu(LocalDate pocetak, LocalDate kraj) {
		LocalDate krajUgovora = datumPocetka.plusMonths(trajanjeUgovora);	
		//ako ni pocetak ni kraj ugovora ne upada u trazeni opseg
		return !((datumPocetka.compareTo(pocetak) < 0) && (krajUgovora.compareTo(pocetak) < 0)) || ((datumPocetka.compareTo(kraj) > 0) && (krajUgovora.compareTo(kraj) > 0));
	}

	
	
	public Predplata(long id, Tarifa tarifa, String preplatnickiBroj, LocalDate datumPocetka, int trajanjeUgovora) {
		this.id = id;
		this.tarifa = tarifa;
		this.preplatnickiBroj = preplatnickiBroj;
		this.datumPocetka = datumPocetka;
		this.trajanjeUgovora = trajanjeUgovora;
	}


	@Override
	public String toString() {
		return "Predplata [id=" + id + ", nazivTarife=" + tarifa.getNaziv() + ", preplatnickiBroj=" + preplatnickiBroj + ", datumPocetka=" + datumPocetka
				+ ", trajanjeUgovora=" + trajanjeUgovora + ", ukupnaCena=" + (trajanjeUgovora * tarifa.getCena()) + "]";
	}


	public Tarifa getTarifa() {
		return tarifa;
	}


	public void setTarifa(Tarifa tarifa) {
		this.tarifa = tarifa;
	}


	public String getPreplatnickiBroj() {
		return preplatnickiBroj;
	}


	public void setPreplatnickiBroj(String preplatnickiBroj) {
		this.preplatnickiBroj = preplatnickiBroj;
	}


	public LocalDate getDatumPocetka() {
		return datumPocetka;
	}


	public void setDatumPocetka(LocalDate datumPocetka) {
		this.datumPocetka = datumPocetka;
	}


	public int getTrajanjeUgovora() {
		return trajanjeUgovora;
	}


	public void setTrajanjeUgovora(int trajanjeUgovora) {
		this.trajanjeUgovora = trajanjeUgovora;
	}


	public long getId() {
		return id;
	}
	
}
