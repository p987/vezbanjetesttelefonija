package model;

import util.Konzola;

public class StavkaIzvestaja {
	public final String nazivTarife ;
	public final double ukupanPrihod;
	public final int trajanjeUgovora;
	public final int brojUgovora;
	public final int aktivniUgovori;
	
	
	public static int compareBrojPoziva(StavkaIzvestaja stavka1, StavkaIzvestaja stavka2) {
		return -Double.compare(stavka1.ukupanPrihod, stavka2.ukupanPrihod);
	}


	public StavkaIzvestaja(String nazivTarife, double ukupanPrihod, int trajanjeUgovora, int brojUgovora, int aktivniUgovori) {
		this.nazivTarife = nazivTarife;
		this.ukupanPrihod = ukupanPrihod;
		this.trajanjeUgovora = trajanjeUgovora;
		this.brojUgovora = brojUgovora;
		this.aktivniUgovori = aktivniUgovori;
	}


	@Override
	public String toString() {
		return "StavkaIzvestaja [nazivTarife=" + nazivTarife + ", " + "aktivniUgovori=" + aktivniUgovori +", "
				+ "trajanjeUgovora=" + trajanjeUgovora + " meseca , odabrano je " + brojUgovora + " puta , ukupanPrihod=" + ukupanPrihod + "]";
	}
	
}
