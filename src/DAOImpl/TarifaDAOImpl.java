package DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import DAO.TarifaDAO;
import model.Predplata;
import model.Tarifa;



public class TarifaDAOImpl implements TarifaDAO {

	private Connection conn;
	
	public TarifaDAOImpl(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Tarifa get(long idTarife) throws Exception{
		Tarifa tarifa = null;
		
		String sql = "SELECT t.id, t.naziv, t.opis, t.cena,\r\n"
				+ "p.id, p.pretplatnickiBroj, p.datumPocetka, p.trajanjeUgovora \r\n"
				+ "FROM tarifa t \r\n"
				+ "LEFT JOIN pretplate p ON t.id = p.tarifaId WHERE t.id = ?;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setLong(1, idTarife);
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long tId = rset.getLong(++kolona);
					String tNaziv = rset.getString(++kolona);
					String tOpis = rset.getString(++kolona);
					double tCena = rset.getDouble(++kolona);
					
					if (tarifa == null) {
						tarifa = new Tarifa(tId, tNaziv, tOpis, tCena);
					}
					
					long pId = rset.getLong(++kolona);
					if (pId != 0) {
						String pPreplatnickiBroj = rset.getString(++kolona);
						LocalDate pDatumPocetka = rset.getDate(++kolona).toLocalDate();
						int pTrajanjeUgovora = rset.getInt(++kolona);

						Predplata preplata = new Predplata(pId, tarifa, pPreplatnickiBroj, pDatumPocetka, pTrajanjeUgovora);
						tarifa.add(preplata);
					}
				}
			}
		}
		return tarifa;
	}

	@Override
	public Collection<Tarifa> getAll() throws Exception{
		Map<Long, Tarifa> tarifaMap = new LinkedHashMap<>();
		
		String sql = "SELECT t.id, t.naziv, t.opis, t.cena,\r\n"
				+ "p.id, p.pretplatnickiBroj, p.datumPocetka, p.trajanjeUgovora \r\n"
				+ "FROM tarifa t \r\n"
				+ "LEFT JOIN pretplate p ON t.id = p.tarifaId;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long tId = rset.getLong(++kolona);
					String tNaziv = rset.getString(++kolona);
					String tOpis = rset.getString(++kolona);
					double tCena = rset.getDouble(++kolona);
					
					Tarifa tarifa = tarifaMap.get(tId);
					if (tarifa == null) {
						tarifa = new Tarifa(tId, tNaziv, tOpis, tCena);
						tarifaMap.put(tarifa.getId(), tarifa);
					}
					
					long pId = rset.getLong(++kolona);
					if (pId != 0) {
						String pPreplatnickiBroj = rset.getString(++kolona);
						LocalDate pDatumPocetka = rset.getDate(++kolona).toLocalDate();
						int pTrajanjeUgovora = rset.getInt(++kolona);

						Predplata preplata = new Predplata(pId, tarifa, pPreplatnickiBroj, pDatumPocetka, pTrajanjeUgovora);
						tarifa.add(preplata);
					}
				}
			}
		}
		return tarifaMap.values();
	}
}
