package DAOImpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import DAO.PreplataDAO;
import model.Predplata;
import model.Tarifa;

;

public class PreplataDAOImpl implements PreplataDAO {
	
	private Connection conn;

	public PreplataDAOImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Collection<Predplata> getAll() throws Exception{
		Map<Long, Predplata> preplataMap = new LinkedHashMap<>();
		
		String sql = "SELECT p.id, p.pretplatnickiBroj, p.datumPocetka, p.trajanjeUgovora, \r\n"
				+ "t.id, t.naziv, t.opis, t.cena\r\n"
				+ "FROM pretplate p  \r\n"
				+ "LEFT JOIN tarifa t ON p.tarifaId = t.id;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long pId = rset.getLong(++kolona);
					String pPreplatnickiBroj = rset.getString(++kolona);
					LocalDate pDatumPocetka = rset.getDate(++kolona).toLocalDate();
					int pTrajanjeUgovora = rset.getInt(++kolona);			
					
					Predplata preplata = preplataMap.get(pId);
					if (preplata == null) {
						preplata = new Predplata(pId, null, pPreplatnickiBroj, pDatumPocetka, pTrajanjeUgovora);
						preplataMap.put(preplata.getId(), preplata);
					}
					
					long tId = rset.getLong(++kolona);
					if (pId != 0) {						
						String tNaziv = rset.getString(++kolona);
						String tOpis = rset.getString(++kolona);
						double tCena = rset.getDouble(++kolona);

						Tarifa tarifa = new Tarifa(tId, tNaziv, tOpis, tCena);
						preplata.setTarifa(tarifa);
					}
				}
			}
		}
		return preplataMap.values();
	}
	
	
	@Override
	public void add(Predplata preplata) throws Exception {
		String sql = "INSERT INTO pretplate (id, tarifaId, pretplatnickiBroj, datumPocetka, trajanjeUgovora) VALUES (?, ?, ?, ?, ?);";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			int param = 0;
			stmt.setLong(++param, preplata.getId());
			stmt.setLong(++param, preplata.getTarifa().getId());
			stmt.setString(++param, preplata.getPreplatnickiBroj());
			stmt.setDate(++param, Date.valueOf(preplata.getDatumPocetka()));			
			stmt.setInt(++param, preplata.getTrajanjeUgovora());
			

			stmt.executeUpdate();
		}
	}

}
