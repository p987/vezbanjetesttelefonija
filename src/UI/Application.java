package UI;

import java.sql.Connection;
import java.sql.DriverManager;

import DAO.PreplataDAO;
import DAO.TarifaDAO;
import DAOImpl.PreplataDAOImpl;
import DAOImpl.TarifaDAOImpl;
import util.Meni;
import util.Meni.FunkcionalnaStavkaMenija;
import util.Meni.IzlaznaStavkaMenija;
import util.Meni.StavkaMenija;




public class Application {
	private static void initDatabase() throws Exception {
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/telefonija?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=Europe/Belgrade", 
				"root", 
				"root");
		
		TarifaDAO tarifaDAO = new TarifaDAOImpl(conn);
		PreplataDAO preplataDAO = new PreplataDAOImpl(conn);
		
		TarifaUI.setTarifaDAO(tarifaDAO);
		PreplataUI.setPreplataDAO(preplataDAO);
		IzvestajUI.setIzvestaj(tarifaDAO);
	}

	static {
		try {
			initDatabase();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Gre�ka pri povezivanju sa izvorom podataka!");
			
			System.exit(1);
		}
	}

	public static void main(String[] args) throws Exception {
		Meni.pokreni("Telefonija", new StavkaMenija[] {
			new IzlaznaStavkaMenija("Izlaz"),
			new FunkcionalnaStavkaMenija("Prikaz svih tarifa;") {

				@Override
				public void izvrsi() { TarifaUI.prikazSvih(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Prikaz svih pretplata sa povezanim tarifama") {

				@Override
				public void izvrsi() { PreplataUI.prikazSvih(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Dodavanje pretplate;") {

				@Override
				public void izvrsi() { PreplataUI.dodavanje(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Izvestaj") {

				@Override
				public void izvrsi() { IzvestajUI.izvestavanje(); }
				
			}
		});
	}
}
