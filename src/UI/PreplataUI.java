package UI;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import DAO.PreplataDAO;
import model.Predplata;
import model.Tarifa;
import util.Konzola;

public class PreplataUI {

	private static PreplataDAO preplataDAO;
	
	public static void setPreplataDAO(PreplataDAO preplataDAO) {
		PreplataUI.preplataDAO = preplataDAO;
	}

	
	public static void prikazSvih() {
		try {
			Collection<Predplata> preplataKolekcija = preplataDAO.getAll();

			System.out.println();
			for (Predplata itPreplata: preplataKolekcija) {
				System.out.println(itPreplata);
//				for (Karta itPorudzbina: itVoz.getKartaSet()) {
//					System.out.println(itPorudzbina);
//				}
//				System.out.println("\n\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
	
	
	public static void dodavanje() {
		try {
			Tarifa tarifa = TarifaUI.pronalazenje();
			Collection<Predplata> preplataKolekcija = preplataDAO.getAll();
			
			if (tarifa == null) {
				return;
			}
			
			String preplatnickiBroj = Konzola.ocitajString("PreplatnickiBroj(broj telefona)");
			for (Predplata itPreplata : preplataKolekcija) {	//pregledam sve preplate
				if (itPreplata.getPreplatnickiBroj().equals(preplatnickiBroj)) {	//ako nadje isti br. telefona
					long trajanjeUgovora = itPreplata.getTrajanjeUgovora();		//prebacim int u long
					if ((itPreplata.getDatumPocetka().plusMonths(trajanjeUgovora)).compareTo(LocalDate.now()) > 0) {	//provera da li jos traje ugovor
						System.out.println("Jos traje ugovor \"" + itPreplata.getTarifa().getNaziv() + "\"");
						return;
					}			
				}
			}
			
			int trajanjeUgovora = Konzola.ocitajInt("Unesite trajanje ugovora(12 ili 24 ili 36 meseci)");
			if (!((trajanjeUgovora == 12) || (trajanjeUgovora == 24) || (trajanjeUgovora == 36))) {
				System.out.println("Trajanje ugovora moze biti 12, 24 ili 36 meseci!)");
				return;
			}

			LocalDate datumPocetka = LocalDate.now();
			Predplata karta = new Predplata(0, tarifa, preplatnickiBroj, datumPocetka, trajanjeUgovora);
			preplataDAO.add(karta);
			System.out.println("Preplata je uspesno dodata!!");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}


	

}
