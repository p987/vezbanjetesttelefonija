package UI;

import java.util.Collection;

import DAO.TarifaDAO;
import model.Predplata;
import model.Tarifa;
import util.Konzola;



public class TarifaUI {

	private static TarifaDAO tarifaDAO;
	
	public static void setTarifaDAO(TarifaDAO vozDAO) {
		TarifaUI.tarifaDAO = vozDAO;
	}
	
	
	public static Tarifa pronalazenje() throws Exception {
		prikazSvih();

		long brojVoza = Konzola.ocitajLong("Unesite id tarife: ");
		System.out.println();

		Tarifa voz = tarifaDAO.get(brojVoza);
		if (voz == null)
			Konzola.prikazi("Voz nije pronađeno!");

		return voz;
	}
	
	
//	public static void prikaz() {
//		try {
//			Tarifa tarifa = pronalazenje();
//			if (tarifa == null)
//				return;
//			
//			System.out.println(tarifa);
//			System.out.println("Broj preostalih slobodnih mesta je: " + (tarifa.getBrojMesta() - tarifa.getKartaSet().size()));
//			System.out.println();
//			for (Predplata itKarta : tarifa.getKartaSet()) {
//				System.out.println(itKarta);
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			System.out.println("Došlo je do greške!");
//		}
//	}

	public static void prikazSvih() {
		try {
			Collection<Tarifa> tarifaKolekcija = tarifaDAO.getAll();

			System.out.println();
			for (Tarifa itTarifa: tarifaKolekcija) {
				System.out.println(itTarifa);
//				for (Karta itPorudzbina: itVoz.getKartaSet()) {
//					System.out.println(itPorudzbina);
//				}
//				System.out.println("\n\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}

	

	

}
