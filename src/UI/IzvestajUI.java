package UI;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import DAO.TarifaDAO;
import model.Predplata;
import model.StavkaIzvestaja;
import model.Tarifa;
import util.Konzola;




public class IzvestajUI {
	private static TarifaDAO tarifaDAO;

	public static void setIzvestaj(TarifaDAO tarifaDAO) {
		IzvestajUI.tarifaDAO = tarifaDAO;
	}
	
	public static void izvestavanje() {
//		LocalDateTime pocetak = Konzola.ocitajDateTime("Unesite pocetak perioda");
//		LocalDateTime kraj = Konzola.ocitajDateTime("Unesite kraj perioda");
		LocalDate pocetak = LocalDate.parse("01.01.2024.", Konzola.getDateFormatter());
		LocalDate kraj =	LocalDate.parse("08.05.2024.", Konzola.getDateFormatter());
		try {
			Collection<Tarifa> tarife = tarifaDAO.getAll();

			// pokupiti sve nazive vozova u jednu kolekciju (Set spreÄ�ava duplikate)
			Set<String> naziviTarife = new LinkedHashSet<>();
			for (Tarifa itTarifa: tarife) {
				naziviTarife.add(itTarifa.getNaziv());
			}
			// generisanje izveÅ¡taja
			List<StavkaIzvestaja> izvestajList = new ArrayList<>();
			for (String itNazivTarife: naziviTarife) { // za svaki naziv voza
				// kolona 2
				double ukupanPrihod = 0; // akumulator
				// kolona 3
				 int brojac1God = 0;
				 int brojac2God = 0;
				 int brojac3God = 0;
				 int najviseBrojac = 0;
				 int aktivniUgovori = 0;
	
				for (Tarifa itTarifa: tarife) { 
					if (itTarifa.getNaziv().equals(itNazivTarife)) {
						for (Predplata itPredplata: itTarifa.getPreplataSet()) { 
							boolean provera = itPredplata.isDatumUOpsegu(pocetak, kraj);
							 if (provera) {
								aktivniUgovori++;
								ukupanPrihod += itPredplata.getTrajanjeUgovora() * itTarifa.getCena();
								if (itPredplata.getTrajanjeUgovora() == 12) {
									++brojac1God;
								} else if (itPredplata.getTrajanjeUgovora() == 24) {
									++brojac2God;
								} else {
									++brojac3God;
								}
							}
						}
						
						if (ukupanPrihod != 0) {
							najviseBrojac = brojac1God;
							int trajanjaUgovora = 12;
							if (brojac2God > najviseBrojac) {
								najviseBrojac = brojac2God;
								trajanjaUgovora = 24;
							}
							if (brojac3God > najviseBrojac) {
								najviseBrojac = brojac3God;
								trajanjaUgovora = 36;
							}
							StavkaIzvestaja stavkaIzvestaja = new StavkaIzvestaja(itTarifa.getNaziv(), ukupanPrihod,
									trajanjaUgovora, najviseBrojac,aktivniUgovori);
							izvestajList.add(stavkaIzvestaja);
						}
					}
				}
				
			}
			izvestajList.sort(StavkaIzvestaja::compareBrojPoziva);

			// prikaz izveÅ¡taja
			System.out.println();
			for (StavkaIzvestaja itStavka : izvestajList) {
				System.out.println(itStavka);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("DoÅ¡lo je do greÅ¡ke!");
		}
	}
}
